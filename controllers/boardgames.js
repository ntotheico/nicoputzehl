// all options are optional
const options = {
  timeout: 10000, // timeout of 10s (5s is the default)
 
  // see https://github.com/cujojs/rest/blob/master/docs/interceptors.md#module-rest/interceptor/retry
  retry: {
    initial: 100,
    multiplier: 2,
    max: 15e3
  }
}
 
const bgg = require('bgg')(options);
const boardgames = [];


bgg('collection', {username: 'nicobert'})
  .then(function(results){
    // console.log(results.items);
    let games = [];
    games = results.items.item;
    console.log(games);
    for(var i = 0; i < games.length; i++) {
    		console.log(games[i].name.$t);
    		getBoardgameData(games[i].name.$t,games[i].thumbnail);
    }
    console.log(boardgames);
  });

function getBoardgameData(title, thumbnail) {
	boardgames.push({'title': title,'image': thumbnail })
// console.log(title);
}


exports.index = function(req, res, next) {
  res.render('boardgames', {boardgames:boardgames, title: 'Meine Brettspiele', pageId: 400 });
}


 
