const letterboxd = require('letterboxd');
const { orderBy } = require('natural-orderby');
const dateFormat = require('dateformat');
const movies = [];
var sortedMovies = [];
const MOVIES_PAGE = 300;
const LOWEST_MOVIE_PAGE = 310;
// const HIGHEST_MOVIE_PAGE = 350;

const constantModule = require('../constants');
const MAXNUMBER = constantModule.HIGHEST_MOVIE_PAGE;


letterboxd('ntotheico')
	.then(function (items) {
	/**/console.log(items);
		for (var i = 0; i < 9; i++) {
			let slug = items[i].film.title.toLowerCase().replace(/\s/g, "-");
			getMovieData(items[i].film.title, items[i].date.watched, items[i].rating.score,items[i].film.image.medium)
		}
		// console.log(movies);
		sortedMovies = orderBy(movies,[v => v.watched],['desc']);
		sortedMovies.forEach(function(movie, i){
			movie.pageId = ((i+1)*10)+MOVIES_PAGE;
			movie.nextId = nextMovieId(movie.pageId);
			movie.prevId = previousMovieId(movie.pageId);
			// console.log(i);
		});
		// console.log(sortedMovies);
	})
	.catch(function (error) {
		console.log(error);
});


function getMovieData(title, watched, score, image, id) {
	let date = dateFormat(new Date(watched), "dd.mm.yyyy"); ;
	movies.push({'title': title,'watched':watched,'myscore':score, 'image': image, 'formatDate':date })
	// console.log(title);
}

function nextMovieId(pageId) {
	if(pageId === MAXNUMBER) {
		return LOWEST_MOVIE_PAGE;
	} else {
		return pageId+10;
	}
}
function previousMovieId(pageId) {
	if(pageId === LOWEST_MOVIE_PAGE) {
		return MAXNUMBER;
	} else {
		return pageId-10;
	}
}

exports.index = function(req, res, next) {
  res.render('movies', {movies:sortedMovies, title:'Zuletzt gesehen', pageId: MOVIES_PAGE});
}

exports.single = function(req, res, next) {
	// console.log('movies single: '+req.params);
	let pageId = parseInt(req.originalUrl.substring(1,4));
	const foundMovie = getMovieByPageId(pageId, sortedMovies);
	console.log('%cFilmtitel:','background:red;color:#fff;padding:10px')
	console.log(foundMovie.filmtitel);
	res.render('movie', foundMovie);
}

 
 const getMovieByPageId =  (pageId, movieList) => {
 	// console.log('getMovieByPageId()');
 	// console.log('pageId: ' +pageId );
 	// console.log('movieList:');
 	// console.log(movieList);
  return movieList.find((movie) => {
    return Number(movie.pageId) === Number(pageId);
  })
};