// Timer für Anzeigge der Uhrzeit
var myVar = setInterval(myTimer, 1000);
var targetPage;

function myTimer() {
  let d = new Date();
  let options = { weekday: 'short', year: '2-digit', month: '2-digit', day: '2-digit' };
  document.getElementById("time").innerHTML = d.toLocaleTimeString();
  document.getElementById("day").innerHTML = d.toLocaleDateString('de-DE', options);
}

//Variable wird verwendet, um sicherzustellen, dass Navigationscounter nur einmal läuft.
var noNavigationNotRunning = true;

// Beschränkt Eingabe auf 3 Zeichen
function checkNumberFieldLength(elem){
    if (elem.value.length > 3) {
        elem.value = elem.value.slice(0,3); 
    }
}

// Navigation auch über Zahlen klickbar
$('.videotext-navigation__number').click(function(){
	$('.breadcrumb__input').val($(this).text()).submit();
})

// Input kann nur bestätig werden, wenn die Eingabe nicht kleiner als 3 ist und das erste Zeichen keine 0 ist
function inputCheck(){
	if(($('.breadcrumb__input').val().length < 3) || (parseInt($('.breadcrumb__input').val().substr(0,1))==0)) {return;}
	if (noNavigationNotRunning){navigation();}
}
	
// Hauptnavigations Funktion
function navigation(){ 
	// ID für das Navigations Intervall
	let navigationID;  
	// Auslesen der Zielseite
	targetPage = parseInt($('.breadcrumb__input').val());
	// Auslesen der aktuellen Seite
	let actualPage = parseInt($('#breadcrumb__location-number').html());
	// Navigation wird auf aktiv gestellt
	noNavigationNotRunning = false;
	// Prüfen, was hier gemacht wird
	$('.breadcrumb__input').blur();
	// Navigations intervall wird gestartet. 10 ist hierbei die Geschwindigkeit
	navigationID = setInterval(countUp, 10);

	// Funktion, mit der das Hochzählen umgesetzt ist
	function countUp() {
		// Wenn beim hochzählen festgestellt wird, dass die aktuelle Seite die Zielseite ist, 
		// wird die Funktion targetReached ausgelöst
		if (actualPage == targetPage) {
			targetReached();
		} 
		// Es wird überprüft ob,
		// die aktuelle Seite 999 ist --> Umbruch auf 100
		// und gleichzeitig die Zielseite 100 ist.
		// die Aktuelle Nummer wird auf 100 gestellt und die Funktion targetReached() ausgelöst
		else if (actualPage == 999 && targetPage == 100) {
			$('#breadcrumb__location-number').html(100);
			targetReached();
		} 
		// Seitenumbruch von Seite 999 auf 100
		else if (actualPage == 999) {
			actualPage = 100;
		} 
		// Seite wird hochgezählt
		else {
			actualPage++; 
			$('#breadcrumb__location-number').html(actualPage);
		}
		// Hier wird der Wert des Inputfelds geleert und in den placeholder geschrieben,
		// die Funktion pageCall wird ausgelöst um das entsprechende Template zuzuweisen
		// der Navigationsindikator wird quasi wieder auf inaktiv gestellt
		// und das Navigationsintervall wird gecleart
		function targetReached() {
			// $('.breadcrumb__input').val('').attr("placeholder", targetPage);
			pageCall(targetPage);
			noNavigationNotRunning = true;
			clearInterval(navigationID);
		}
	}
};



// Seitenzuweisung und Templateaufruf
function pageCall(targetPage) {
	console.log('targetPage: '+targetPage);
	let target = '/'+targetPage;
	// nur in pages enthalten Seiten werden navigiert
	let pages = [100,300,310,320,330,340,350,360,370,380,390,400,800,900,950];
	if(pages.includes(targetPage)){
		if(targetPage ===100) {
			window.location.assign('/');
		} else {
			window.location.assign(target);
		}
	} else{
		window.location.assign('/666');
	}
}

// OnLoad wird der Placeholder gesetzt
$(function() {
	setBreadCrumbPlaceholder();

	console.log('Pathname');
	console.log(window.location.pathname);
	if (window.location.pathname =='/666') {
		$('#main').addClass('flex-center');
	}else {
		$('#main').removeClass('flex-center');
	}

	if (scrollPage()) {
		$('main').addClass('scroll');
	} else {
		$('main').removeClass('scroll');
	}

	if (window.location.pathname ==='/') {
		targetPage = 100;
	}
});
function setBreadCrumbPlaceholder(){
	let currentPage = parseInt($('#breadcrumb__location-number').html());
	$('.breadcrumb__input').attr('placeholder',currentPage)
}

function scrollPage() {
	// if (window.location.pathname =='/950' 
	// 	|| window.location.pathname =='/666') {
	// 	return true;
	// } else {
		return false;
	// }

}
