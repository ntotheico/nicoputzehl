var express = require('express');
var router = express.Router();

let index = require('../controllers/index');
let contact = require('../controllers/contact');
let movies = require('../controllers/movies');
let privacy = require('../controllers/privacy');
let impress = require('../controllers/impress');
let viernullvier = require('../controllers/viernullvier');


exports.index = function(req, res, next) {
	console.log('vt routing index() -> req.params.pageId');
	console.log(req.params.pageId);
  // res.render('movies', {movies:sortedMovies, title:'Zuletzt gesehen', pageId: MOVIES_PAGE});
  	switch(req.params.pageId) {
  	    case 100: return router.get('/', index.index);
    		break;	    
		case 300: return router.get('/movies', movies.index);
    		break;		    
		case 310: return router.get('movies/:pageId', movies.single);
    		break;		    
		case 320: return router.get('movies/:pageId', movies.single);
    		break;			    
		case 330: return router.get('movies/:pageId', movies.single);
    		break;			    
		case 340: return router.get('movies/:pageId', movies.single);
    		break;			    
		case 350: return router.get('movies/:pageId', movies.single);
    		break;		
		case 800: return router.get('/contact', contact.index);
    		break;	
		case 900: return router.get('/privacy', privacy.index);
    		break;	
		case 950: return router.get('/impress', impress.index);
    		break;	    
		default: return router.get('/viernullvier', viernullvier.index);
    		break;
    	}
}








