var express = require('express');
var router = express.Router();

let index = require('../controllers/index');
let contact = require('../controllers/contact');
let movies = require('../controllers/movies');
let boardgames = require('../controllers/boardgames');
let privacy = require('../controllers/privacy');
let impress = require('../controllers/impress');
let viernullvier = require('../controllers/viernullvier');

var constantModule = require('../constants');
var maxNumber = constantModule.HIGHEST_MOVIE_PAGE;

router.get('/', index.index);

router.get('/300', movies.index);
for (var i = 310; i <= maxNumber; i = i + 10) {
	router.get('/'+i, movies.single);
	console.log(i);
}
router.get('/400', boardgames.index);
// router.get('/310', movies.single);
// router.get('/320', movies.single);
// router.get('/330', movies.single);
// router.get('/340', movies.single);
// router.get('/350', movies.single);

router.get('/800', contact.index);
router.get('/900', impress.index);
router.get('/950', privacy.index);

router.get('/666', viernullvier.index);

// router.get('/contact', contact.index);
// router.get('/privacy', privacy.index);
// router.get('/movies/:pageId', movies.single);
// router.get('/impress', impress.index);
// router.get('/viernullvier', viernullvier.index);

module.exports = router;