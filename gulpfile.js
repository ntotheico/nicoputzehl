var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function(){
  return gulp.src('sass/style.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('build/css'))
    .pipe(gulp.dest('public/stylesheets'))
});

// Gulp watch syntax
gulp.task('watch', function() {
  gulp.watch('sass/**/*.scss', gulp.series('sass'));
});